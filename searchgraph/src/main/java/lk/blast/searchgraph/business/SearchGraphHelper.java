/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blast.searchgraph.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import lk.blast.searchgraph.common.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author lk
 */
public class SearchGraphHelper {

    final Logger logger = LogManager.getLogger(SearchGraphHelper.class.getName());
    URL url = ClassLoader.getSystemResource(Constants.GRAPH_BACKUP_FILE);

    public void backupGraph(SearchGraphBusiness obj) {
        FileOutputStream fos;
        ObjectOutputStream out;
        try {
            File f = new File(url.getPath());
            fos = new FileOutputStream(f);
            out = new ObjectOutputStream(fos);
            out.writeObject(obj);
            out.close();
        } catch (IOException ex) {
            logger.error("Error serializing Object. Failed to backup Graph.");
        }
    }

    public SearchGraphBusiness restoreGraph() throws IOException {
        FileInputStream fis;
        ObjectInputStream in;
        SearchGraphBusiness obj = SearchGraphBusiness.getInstance();
        try {
            fis = new FileInputStream(url.getPath());
            in = new ObjectInputStream(fis);
            obj = (SearchGraphBusiness) in.readObject();
            in.close();
        } catch (ClassNotFoundException | IOException ex) {
            logger.error("Error deserializing Object. Failed to restore Graph.");
            throw new IOException();
        }
        return obj;

    }

}
