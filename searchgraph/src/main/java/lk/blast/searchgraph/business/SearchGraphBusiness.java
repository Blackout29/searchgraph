/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blast.searchgraph.business;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import lk.blast.searchgraph.common.Constants;
import lk.blast.searchgraph.value.NodeVO;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * Search graph built using JgraphT root is always football - Then there are -
 *
 *
 * @author lk
 */
public class SearchGraphBusiness implements Serializable {

    private SimpleWeightedGraph<String, DefaultWeightedEdge> searchGraph;

    private static SearchGraphBusiness instance = null;

    public static SearchGraphBusiness getInstance() {
        if (instance == null) {
            SearchGraphHelper sgh = new SearchGraphHelper();
            try {
                instance = sgh.restoreGraph();
            } catch (IOException ex) {
                instance = new SearchGraphBusiness();
            }
        }
        return instance;
    }

    private SearchGraphBusiness() {
        searchGraph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        //add root
        searchGraph.addVertex(Constants.GRAPH_ROOT);
    }

    public JSONObject add(NodeVO sourceVO, NodeVO targetVO, double weight) {
        String source = sourceVO.getNode();
        String target = targetVO.getNode();
        JSONObject jo = new JSONObject();
        DefaultWeightedEdge edge;

        try {
            if (searchGraph.containsVertex(source)) {
                if (searchGraph.containsVertex(target)) {
                    if (searchNode(target) > searchNode(source)) {
                        edge = searchGraph.addEdge(source, target);
                        searchGraph.setEdgeWeight(edge, weight);
                        jo.put(Constants.RETURN_CODE, Constants.SUCCESS);
                    } else {
                        jo.put(Constants.RETURN_CODE, Constants.NODE_EXISTS);
                    }
                } else {
                    //target doesn't exists
                    searchGraph.addVertex(target);
                    edge = searchGraph.addEdge(source, target);
                    searchGraph.setEdgeWeight(edge, weight);
                    jo.put(Constants.RETURN_CODE, Constants.SUCCESS);
                }
            } else {
                jo.put(Constants.RETURN_CODE, Constants.SOURCE_NOT_EXISTS);
            }
        } catch (JSONException jex) {
            jex.printStackTrace();
        }

        System.out.println(searchGraph.toString());
        return jo;
    }

    private void deleteNode(String source) {

    }

    public JSONArray searchNodes(List<NodeVO> nodeVOs) {

        JSONArray ja = new JSONArray();
        for (NodeVO aNodeVO : nodeVOs) {
            JSONObject jo = new JSONObject();
//            distance = sg.searchNode(aNodeVO.getNode());
            try {
                jo.put(Constants.NODE, aNodeVO.getNode());
                jo.put(Constants.DISTANCE, searchNode(aNodeVO.getNode()));
            } catch (JSONException jex) {
                jex.printStackTrace();
            }

            ja.put(jo);
        }
        return ja;
    }

    public double searchNode(String target) {
        double distance;
        try {
//            distance = DijkstraShortestPath.findPathBetween(searchGraph, Constants.GRAPH_ROOT, target).size() + 1;
            DijkstraShortestPath dsp = new DijkstraShortestPath(searchGraph, Constants.GRAPH_ROOT, target);
            distance = dsp.getPathLength();
        } catch (IllegalArgumentException ex) {
            distance = 0;
        }
        return distance;
    }

    public void buildFromsolr() {

    }

}
