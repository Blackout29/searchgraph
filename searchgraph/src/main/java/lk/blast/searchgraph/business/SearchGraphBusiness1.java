/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blast.searchgraph.business;

import lk.blast.searchgraph.common.Constants;
import java.util.List;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 * Search graph built using JgraphT
 * root is always football - 
 * Then there are - 
 * 
 * 
 * @author lk
 */
public class SearchGraphBusiness1 {

    private SimpleWeightedGraph<String, DefaultWeightedEdge> searchGraph;
     
    private static SearchGraphBusiness1 instance = null;

    public static SearchGraphBusiness1 getInstance() {
        if (instance == null) {
            instance = new SearchGraphBusiness1();
        }
        return instance;
    }

    private SearchGraphBusiness1() {
        searchGraph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        //add root
        searchGraph.addVertex(Constants.GRAPH_ROOT);
    }

    public JSONObject addNode(String source, String target) {
        JSONObject jo = new JSONObject();
        try {
            if (searchGraph.containsVertex(source)) {
                if (searchGraph.containsVertex(target)) {
                    if (searchNode(target) > (searchNode(source) + 1)) {
                        searchGraph.addEdge(source, target);
                        jo.put(Constants.RETURN_CODE, Constants.SUCCESS);
                    } else {
                        jo.put(Constants.RETURN_CODE, Constants.NODE_EXISTS);
                    }
                } else {
                    //target doesn't exists
                    searchGraph.addVertex(target);
                    searchGraph.addEdge(source, target);
                    jo.put(Constants.RETURN_CODE, Constants.SUCCESS);
                }
            } else {
                jo.put(Constants.RETURN_CODE, Constants.SOURCE_NOT_EXISTS);
            }
        } catch (JSONException jex) {
            jex.printStackTrace();
        }

        System.out.println(searchGraph.toString());
        return jo;
    }

    private void deleteNode(String source) {

    }

    public int searchNode(String target) {
        JSONObject jo = new JSONObject();
        List edges = null;
        int distance;
        try {
            distance = DijkstraShortestPath.findPathBetween(searchGraph, Constants.GRAPH_ROOT, target).size() + 1;
        } catch (IllegalArgumentException ex) {
            distance = 0;
        }
        return distance;
    }

    public static void main(String[] args) {
        SearchGraphBusiness1 ins = getInstance();
        
        ins.addNode("football", "");
    }
    
    
}
