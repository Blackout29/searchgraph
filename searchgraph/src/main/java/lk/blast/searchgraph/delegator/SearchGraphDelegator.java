/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.blast.searchgraph.delegator;

import java.util.List;
import lk.blast.searchgraph.business.SearchGraphBusiness;
import lk.blast.searchgraph.value.NodeVO;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
public class SearchGraphDelegator {
    
    public JSONArray searchNodes(List<NodeVO> nodeVOs) {
        SearchGraphBusiness sg = SearchGraphBusiness.getInstance();
        return sg.searchNodes(nodeVOs);
    }
    
    public JSONObject add(NodeVO sourceVO, NodeVO targetVO, double weight) {
        SearchGraphBusiness sg = SearchGraphBusiness.getInstance();
        return sg.add(sourceVO, targetVO, weight);
    }
    
    public void buildFromSolr() {
        SearchGraphBusiness sg = SearchGraphBusiness.getInstance();
        sg.buildFromsolr();
    }
    
}
