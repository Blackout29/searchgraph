/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blast.searchgraph.restapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lk.blast.searchgraph.common.Constants;
import lk.blast.searchgraph.delegator.SearchGraphDelegator;
import lk.blast.searchgraph.value.NodeVO;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
@Path("/graph")
public class GraphResource {

//    @Context
//    HttpContext context;
    @GET
//    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchNodes(@QueryParam("node") Set<String> nodes) {
        List<NodeVO> nodeVOs = new ArrayList<>();
        for (String node : nodes) {
            NodeVO aNodeVO = new NodeVO();
            aNodeVO.setNode(node);
            nodeVOs.add(aNodeVO);
        }
        SearchGraphDelegator searchGraphDelegator = new SearchGraphDelegator();

        return Response.ok(searchGraphDelegator.searchNodes(nodeVOs)).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(@QueryParam("data") String json) {

        JSONObject jo;
        try {
            jo = new JSONObject(json);
            double weight = jo.getDouble("weight");
            NodeVO sourceVO = new NodeVO(jo.getString("source"));
            NodeVO targetVO = new NodeVO(jo.getString("target"));
            SearchGraphDelegator searchGraphDelegator = new SearchGraphDelegator();
            searchGraphDelegator.add(sourceVO, targetVO, weight);
        } catch (JSONException ex) {
            jo = new JSONObject();
            try {
                jo.put(Constants.RETURN_CODE, "ERROR");
            } catch (JSONException ex1) {
                //do nothing
            }
        }

        return Response.ok(jo).build();
    }

    @GET
    @Path("/build-from-solr")
    @Produces(MediaType.APPLICATION_JSON)
    public Response statusUpdate() {
        SearchGraphDelegator searchGraphDelegator = new SearchGraphDelegator();
        searchGraphDelegator.buildFromSolr();
        return Response.ok("done").build();
    }
}
