/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.blast.searchgraph.common;

/**
 *
 * @author lk
 */
public class Constants {
    public static final String GRAPH_BACKUP_FILE = "graph-backup.obj";
    
    public static final String SUCCESS = "SUCCESS";
    public static final String RETURN_CODE = "return_code";
    
    public static final String GRAPH_CACHE_DIR = "/Users/lk/Thesis/datacache/searchgraph";
    public static final String GRAPH_ROOT = "football";
    public static final String NODE_EXISTS = "Already exists";
    public static final String SOURCE_NOT_EXISTS = "Source does not exists";
    public static final String DISTANCE = "distance";
    public static final String NODE = "token";
    public static final String NODE_NOT_FOUND = "Not found";
    
}
